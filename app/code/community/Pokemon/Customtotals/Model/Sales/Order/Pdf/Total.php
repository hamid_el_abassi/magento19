<?php

class Pokemon_Customtotals_Model_Sales_Order_Pdf_Total extends Mage_Sales_Model_Order_Pdf_Total_Default {

    public function getTotalsForDisplay() {

        $amount = $this->getOrder()->formatPriceTxt($this->getAmount());
        //$amount1 = $this->getOrder()->formatPriceTxt($this->getFixedFee()); empty

        $fontsize = $this->getFontSize() ? $this->getFontSize() : 7;

        $total = array(
            'amount'    => $amount,
            'label'     => 'Hamid discount: ',
            'font_size' => $fontsize
        );
        return array($total);

    }

}