<?php

class Pokemon_Customtotals_Model_Sales_Quote_Address_Total_Fixedfee extends Mage_Sales_Model_Quote_Address_Total_Abstract {

    protected $_code = 'fixedfee';

    public function collect(Mage_Sales_Model_Quote_Address $address) {

        parent::collect($address);

        if ($address->getAddressType() == 'shipping') {

            $fee = 15;

            $address->setFixedFee($fee);
            $address->setBaseFixedFee($fee);

            $address->setGrandTotal($address->getGrandTotal() - $address->getFixedFee());
            $address->setBaseGrandTotal($address->getBaseGrandTotal() - $address->getBaseFixedFee());
        }

    }

    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {

        $amt = $address->getFixedFee();

        if ($amt > 0 && $address->getAddressType() == 'shipping') {

            $address->addTotal(array(
                'code'=> $this->getCode(),
                'title'=> Mage::helper('pokemon_customtotals')->__('My Fee2'),
                'value'=> 15
            ));

        }

        return $this;
    }

}