<?php

class Pokemon_Customtotals_Model_Total_Creditmemo_Fixedfee extends Mage_Sales_Model_Order_Total_Abstract
{
/**
* Collect total when create Creditmemo
*
* @param Mage_Sales_Model_Order_Creditmemo $creditmemo
*/

    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        $order = $creditmemo->getOrder();
        $fixedfee = $order->getFixedFee();
        $basefixedfee = $order->getBaseFixedFee();

        if ($fixedfee <= 0.0 && $basefixedfee <= 0.0) {
            return $this;
        }

        $creditmemo->setFixedFee($fixedfee);
        $creditmemo->setBaseFixedFee($basefixedfee);

        $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() - $fixedfee);
        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() - $basefixedfee);
        return $this;
    }
}