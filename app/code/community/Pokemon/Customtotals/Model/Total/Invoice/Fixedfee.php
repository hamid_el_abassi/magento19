<?php

class Pokemon_Customtotals_Model_Total_Invoice_Fixedfee extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    /**
     * Collect total when create Creditmemo
     *
     * @param Mage_Sales_Model_Order_Creditmemo $creditmemo
     */

    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        $order = $invoice->getOrder();

        $fixedfee = $order->getFixedFee();
        $basefixedfee = $order->getBaseFixedFee();

        if ($fixedfee <= 0.0 && $basefixedfee <= 0.0) {
            return $this;
        }

        $invoice->setFixedFee($fixedfee);
        $invoice->setBaseFixedFee($basefixedfee);

        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() - $fixedfee);
        $invoice->setGrandTotal($invoice->getGrandTotal() - $basefixedfee);
        return $this;
    }
}