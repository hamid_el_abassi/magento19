<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$connection = $installer->getConnection();

$installer->startSetup();

$connection->addColumn($installer->getTable('sales/order'),
        'fixed_fee',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => true,
            'default' => null,
            'comment' => 'Fee Amount'
        )
    );
$connection->addColumn($installer->getTable('sales/order'),
        'base_fixed_fee',
        array(
            'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'nullable' => true,
            'default' => null,
            'comment' => 'Base Fee Amount'
        )
    );

$connection->addColumn($installer->getTable('sales/order_item'),
    'fixed_fee',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'comment' => 'Fixed fee'
    )
);
$connection->addColumn($installer->getTable('sales/order_item'),
    'base_fixed_fee',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'comment' => 'Fixed fee'
    )
);

$connection->addColumn($installer->getTable('sales/invoice'),
    'fixed_fee',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'comment' => 'Fixed fee'
    )
);
$connection->addColumn($installer->getTable('sales/invoice'),
    'base_fixed_fee',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'comment' => 'Fixed fee'
    )
);

$connection->addColumn($installer->getTable('sales/creditmemo'),
    'fixed_fee',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'comment' => 'Fixed fee'
    )
);
$connection->addColumn($installer->getTable('sales/creditmemo'),
    'base_fixed_fee',
    array(
        'type' => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'nullable' => true,
        'default' => null,
        'comment' => 'Fixed fee'
    )
);

$installer->endSetup();

