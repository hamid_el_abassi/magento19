<?php
class Pokemon_Games_Adminhtml_RedController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction() {

        $this->loadLayout()
            ->_setActiveMenu('pokemon/games_jake')
            ->_addBreadcrumb('Pokemon', 'Jake');

        return $this;
    }

    public function indexAction() {
//        echo "blue";

        $this->_initAction();

        $block = $this->getLayout()
            ->createBlock('core/text', 'example-block')
            ->setText('<h1>This is Jake Index</h1>');
        $this->getLayout()->getBlock('content')->append($block);

        $this->renderLayout();

    }

    public function newAction() {

        $this->_forward('edit');

    }

    public function editAction() {

        $id     = $this->getRequest()->getParam('id');
        $model  = Mage::getModel('pokemon_games/jake')->load($id);

        if ($model->getId() || $id == 0) {
            $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
            if (!empty($data)) {
                $model->setData($data);
            }

            Mage::register('pokemon_games', $model);

            $this->loadLayout();
            $this->_setActiveMenu('pokemon/games_jake');

            $this->_addBreadcrumb(Mage::helper('pokemon_games')->__('Item Manager'), Mage::helper('pokemon_games')->__('Item Manager'));
            $this->_addBreadcrumb(Mage::helper('pokemon_games')->__('Item News'), Mage::helper('pokemon_games')->__('Item News'));

            $this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

            $this->_addContent($this->getLayout()->createBlock('games/adminhtml_red_edit'))
                ->_addLeft($this->getLayout()->createBlock('games/adminhtml_red_edit_tabs'));

            $this->renderLayout();

        } else {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pokemon_games')->__('Item does not exist'));
            $this->_redirect('*/*/');
        }

    }

    public function saveAction() {
        if ($data = $this->getRequest()->getPost())
        {
            $model = Mage::getModel('pokemon_games/jake');
            $id = $this->getRequest()->getParam('id');
            foreach ($data as $key => $value)
            {
                if (is_array($value))
                {
                    $data[$key] = implode(',',$this->getRequest()->getParam($key));
                }
            }


            if ($id) {
                $model->load($id);
            }
            $model->setData($data);
            Mage::getSingleton('adminhtml/session')->setFormData($data);
            try {
                if ($id) {
                    $model->setId($id);
                }
                $model->save();
                if (!$model->getId()) {
                    Mage::throwException(Mage::helper('pokemon_games')->__('Error saving news details'));
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pokemon_games')->__('Details was successfully saved.'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
// The following line decides if it is a "save" or "save and continue"
                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                } else {
                    $this->_redirect('*/*/');
                }
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                if ($model && $model->getId()) {
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                } else {
                    $this->_redirect('*/*/');
                }
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pokemon_games')->__('No data found to save'));
        $this->_redirect('*/*/');
    }

    public function deleteAction() {

        echo "id: " . $this->getRequest()->getParam('id');

        if ($id = $this->getRequest()->getParam('id')) {
            try {
                $model = Mage::getModel('pokemon_games/jake');
                $model->setId($id);
                $model->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('pokemon_games')->__('Item has been deleted.'));
                $this->_redirect('*/*/');
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Item can not be found'));
        $this->_redirect('*/*/');

    }

    function redajaxTab() {

        $this->loadLayout();
        $block = $this->getLayout()
            ->createBlock('core/text', 'example-block')
            ->setText('<h1>This is Jake Index</h1>');
        $this->getResponse()->setBody($this->getLayout->createBlock($block)->toHtml());

    }


}
