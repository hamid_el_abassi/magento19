<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'foo_bar_baz'
 */
$table = $installer->getConnection()
    // The following call to getTable('foo_bar/baz') will lookup the resource for foo_bar (foo_bar_mysql4), and look
    // for a corresponding entity called baz. The table name in the XML is foo_bar_baz, so ths is what is created.
    ->newTable($installer->getTable('pokemon_games/jake'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'ID')
    ->addColumn('adwords', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
        'nullable'  => false,
    ), 'Adwords')
    ->addColumn('cpc', Varien_Db_Ddl_Table::TYPE_INTEGER, 0, array(
        'nullable'  => false,
    ), 'CPC')
    ->addColumn('status', Varien_Db_Ddl_Table::TYPE_BOOLEAN, 0, array(
        'nullable'  => false,
    ), 'Status');

$installer->getConnection()->createTable($table);

//$installer->run("
//
//CREATE TABLE IF NOT EXISTS `jake_info` (
//  `id` int(11) NOT NULL AUTO_INCREMENT,
//  `name` varchar(128) DEFAULT NULL,
//  PRIMARY KEY (`id`)
//) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
//
//");

$installer->endSetup();





