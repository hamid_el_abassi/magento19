<?php

class Pokemon_Games_Model_Adminhtml_Source_Status
{
    public function toOptionArray()
    {
        $status = array(
            array('value' => '-1', 'label' => 'Please Select..'),
            array('value' => '1', 'label' => 'Enabled'),
            array('value' => '2', 'label' => 'Disabled')
        );

        return $status;
    }

    public function toOptionHash()
    {
        $status = array(
            '-1' => 'Please Select..',
            '1' => 'Enabled',
            '2' => 'Disabled'
        );

        return $status;
    }

}