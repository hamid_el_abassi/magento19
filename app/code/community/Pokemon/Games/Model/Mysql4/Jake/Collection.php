<?php

class Pokemon_Games_Model_Mysql4_Jake_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract {

    protected function _construct() {

        $this->_init('pokemon_games/jake');

    }

}