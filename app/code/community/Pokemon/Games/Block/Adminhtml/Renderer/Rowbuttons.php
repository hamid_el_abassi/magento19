<?php

class Pokemon_Games_Block_Adminhtml_Renderer_Rowbuttons extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $url = $this->getUrl('*/*/edit', array('id' => $row->getId()));
        $link = '<a href="'.$url.'">Wijzig</a>';

        $url2 = $this->getUrl('*/*/delete', array('id' => $row->getId()));
        $link .= ' <a href="'.$url2.'">Verwijder</a>';

        return $link;
    }
}