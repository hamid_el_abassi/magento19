<?php

class Pokemon_Games_Block_Adminhtml_Red extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'games';
        $this->_controller = 'adminhtml_red';
        $this->_headerText = 'Red grid container';
        $this->_addButtonLabel = 'Add item';

        $this->_addButton('button2', array(
            'label'     => Mage::helper('pokemon_games')->__('Button Label2'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/button2') .'\')',
            'class'     => 'remove',
        ));

        parent::__construct();

    }

}