<?php

class Pokemon_Games_Adminhtml_Blue_Edit_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('blue_tab_form', array(
            'legend' => 'Blue edit tab form'
        ));

        $fieldset->addField('title','text', array(
            'label' => 'Title',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'title'
        ));

        $fieldset->addField('title','text', array(
            'label' => 'Adwords',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'adwords'
        ));

        return parent::_prepareForm();

    }

}
