<?php

class Pokemon_Games_Block_Adminhtml_Blue_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {

        parent::__construct();

        $this->setId('blueGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);

    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('pokemon_games/izzy')->getCollection();
//        $collection = Mage::getResourceModel('pokemon_games/jake_collection');

        $this->setCollection($collection);

        return parent::_prepareCollection();

    }

    protected function _prepareColumns() {

        $this->addColumn('id', array(
           'header' => 'ID',
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
//            'url' => 'controllername',
//            'filter' => 'true',
//            'sortable' => 'true',
//            'type' => 'options'

        ));

        $this->addColumn('adwords', array(
           'header' => 'Adwords',
            'align' => 'center',
            'width' => '150px',
            'index' => 'adwords'
        ));

        $this->addColumn('cpc', array(
           'header' => 'CPC',
           'align' => 'right',
            'width' => '50px',
            'index' => 'cpc'
        ));

        $this->addColumn('status', array(
           'header' => 'Status',
           'align' => 'left',
           'width' => '150px',
           'type' => 'options',
           'options' => array('1' => 'Enabled', '2' => 'Disabled')
        ));

    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit',array('id' => $row->getId()));

    }

}