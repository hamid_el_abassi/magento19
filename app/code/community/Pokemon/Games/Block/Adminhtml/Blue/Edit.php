<?php

class Pokemon_Games_Block_Adminhtml_Blue_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'pokemon_games';
        $this->_controller = 'adminhtml_blue';
        $this->_headerText = 'Red grid container';
        $this->_mode = 'edit';

        $this->_updateButton('save','label','Opslaan Blue');
        $this->_updateButton('delete','label','Verwijder Blue');

        $this->_addButton('saveandcontinue', array(
            'label' => 'Opslaan en doorgaan',
            'onclick' => 'saveAndContinueEdit()',
            'class', 'save',
        ), -100);

    }

}