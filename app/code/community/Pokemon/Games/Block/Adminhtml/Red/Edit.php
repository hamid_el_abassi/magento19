<?php

class Pokemon_Games_Block_Adminhtml_Red_Edit extends Mage_Adminhtml_Block_Widget_Form_Container {

    public function __construct() {

        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'games';
//        $this->_headerText = 'Red widget container';
        $this->_controller = 'adminhtml_red';
        $this->_mode = 'edit';

        $this->_updateButton('save','label','Jake Opslaan');
        $this->_updateButton('delete','label','Jake Verwijder');

        $this->_addButton('saveandcontinue', array(
            'label' => 'Jake Opslaan en doorgaan',
            'onclick' => 'saveAndContinueEdit()',
            'class', 'save',
        ), -100);

    }

    public function getHeaderText() {

        return 'Red widget container';

    }



}