<?php

class Pokemon_Games_Block_Adminhtml_Red_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs {

    public function __construct() {

        parent::__construct();
        $this->setId('jake_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle('Jake information');

    }

    protected function _beforeToHtml() {

        $this->addTab('form_section', array(
            'label'     => Mage::helper('rating')->__('Adwords-label'),
            'title'     => Mage::helper('rating')->__('Adwords-title'),
            'content'   => $this->getLayout()->createBlock('games/adminhtml_red_edit_tab_form')->toHtml()
        ));

        $this->addTab('form_ajax', array(
            'label'     => Mage::helper('rating')->__('Adwords-ajax'),
            'title'     => Mage::helper('rating')->__('adwords-ajax'),
            'url'       => $this->getUrl('*/*/redajaxTab', array('_current' => true)),
            'class'     => 'ajax'
        ));

        return parent::_beforeToHtml();
    }


}