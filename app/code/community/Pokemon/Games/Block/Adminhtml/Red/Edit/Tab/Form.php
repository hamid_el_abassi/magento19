<?php

class Pokemon_Games_Block_Adminhtml_Red_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        if (Mage::registry('pokemon_games')) {
            $data = Mage::registry('pokemon_games')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('red_tab_form', array(
            'legend' => 'Red edit tab form'
        ));

        $fieldset->addField('adwords','text', array(
            'label' => 'Adwords',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'adwords'
        ));

        $fieldset->addField('cpc','text', array(
           'label' => 'CPC',
            'onclick' => "alert('clicked')",
            'onchange' => "alert('changed')",
            'class' => 'required-entry',
            'required' => true,
            'name' => 'cpc'
        ));

        $fieldset->addField('status','select', array(
            'label' => 'Status',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'status',
            'values' => Mage::getModel('pokemon_games/adminhtml_source_status')->toOptionArray(),
        ));

        $form->setValues($data);

        return parent::_prepareForm();

    }

}