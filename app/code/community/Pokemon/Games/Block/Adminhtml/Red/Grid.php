<?php

class Pokemon_Games_Block_Adminhtml_Red_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {

        parent::__construct();

        $this->setId('redGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);

    }

    protected function _prepareCollection() {

        $collection = Mage::getModel('pokemon_games/jake')->getCollection();
//        $collection = Mage::getResourceModel('pokemon_games/jake_collection');

        $this->setCollection($collection);

        return parent::_prepareCollection();

    }

    protected function _prepareColumns() {

        $this->addColumn('id', array(
           'header' => 'ID',
            'align' => 'right',
            'width' => '50px',
            'index' => 'id',
//            'url' => 'controllername',
//            'filter' => 'true',
//            'sortable' => 'true',
//            'type' => 'options'

        ));

        $this->addColumn('adwords', array(
           'header' => 'Adwords',
            'align' => 'center',
            'width' => '150px',
            'index' => 'adwords',
            'renderer'  => 'Pokemon_Games_Block_Adminhtml_Renderer_Rowcolors'
        ));

        $this->addColumn('cpc', array(
           'header' => 'CPC',
           'align' => 'right',
            'width' => '50px',
            'index' => 'cpc'
        ));

        $this->addColumn('status', array(
           'header' => 'Status',
            'index' => 'status',
           'align' => 'left',
           'width' => '150px',
           'type' => 'options',
           'options' => Mage::getModel('pokemon_games/adminhtml_source_status')->toOptionHash(),
        ));

        $this->addColumn('action', array(
                'header' => Mage::helper('pokemon_games')->__('Action'),
                'width' => '100',
                'type' => 'action',
                'getter' => 'getId',
//                'actions' => array(
//                    array(
//                        'caption' => Mage::helper('pokemon_games')->__('View'),
//                        'url' => array('base'=> '*/*/view'),
//                        'field' => 'id'
//                    ),
//                    array(
//                        'caption' => Mage::helper('pokemon_games')->__('Edit'),
//                        'url' => array('base'=> '*/*/edit'),
//                        'field' => 'id2'
//                    ),
//                    array(
//                        'caption' => Mage::helper('pokemon_games')->__('Delete'),
//                        'url' => array('base'=> '*/*/delete'),
//                        'field' => 'id3'
//                    )
//                ),
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'is_system' => true,
                'renderer'  => 'Pokemon_Games_Block_Adminhtml_Renderer_Rowbuttons'
            )
        );



    }

    public function getRowUrl($row) {
        return $this->getUrl('*/*/edit',array('id' => $row->getId()));
    }

}