<?php

class Pokemon_Games_Block_Adminhtml_Red_Edit_Tab_redajaxForm extends Mage_Adminhtml_Block_Widget_Form {

    protected function _prepareForm() {

        if (Mage::registry('pokemon_games')) {
            $data = Mage::registry('pokemon_games')->getData();
        } else {
            $data = array();
        }

        $form = new Varien_Data_Form();
        $this->setForm($form);

        $fieldset = $form->addFieldset('red_ajaxtab_form', array(
            'legend' => 'Red ajax tab form'
        ));

        $fieldset->addField('visitor','text', array(
            'label' => 'Visitor',
            'class' => 'required-entry',
            'required' => true,
            'name' => 'visitor'
        ));

        $fieldset->addField('location','text', array(
           'label' => 'Location',
            'onclick' => "alert('clicked')",
            'onchange' => "alert('changed')",
            'class' => 'required-entry',
            'required' => true,
            'name' => 'location',
            'style' => 'bgcolor: #ccc',
            'after_element_html' => '<input type="checkbox" name="ajaxbox" title="ajaxbox">'
        ));

        $form->setValues($data);

        return parent::_prepareForm();

    }

}