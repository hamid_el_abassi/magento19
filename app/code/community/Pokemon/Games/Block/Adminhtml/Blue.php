<?php

class Pokemon_Games_Block_Adminhtml_Blue extends Mage_Core_Block_Template
{
    public function __construct()
    {
        $this->_blockGroup = 'pokemon_games';
        $this->_controller = 'adminhtml_blue';
        $this->_headerText = 'Blue grid container';
        $this->_addButtonLabel = 'Add item';

        parent::__construct();

    }
}